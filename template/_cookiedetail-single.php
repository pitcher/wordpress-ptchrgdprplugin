<?php if($script['visible']):?>

<div class="cookiedetail__single">
    <div class="toggleicon"><img src="<?php echo PTCHR_GDPR_PLUGIN_DIRECTORY ;?>template/assets/svg/chevron-arrow-down.svg"> </div>
    <div class="cookiedetail__content"><?php echo $script['name'];?></div>

    <div class="cookiedetail__content">
        <?php
        switch ($script['script_type']) :
            case 0:
                echo $strings['necessary-title'];
                break;
            case 1:
                echo $strings['preferences-title'];
                break;
            case 2:
                echo $strings['statistics-title'];
                break;
            case 3:
                echo $strings['marketing-title'];
                break;
        endswitch;

        ?>
    </div>

    <div class="cookiedetail__content cookiedetail__content--large">

        <div class=""><?php echo $strings['duration'];?>: <?php echo $script['duration'];?>

            <?php
            switch ($script['duration_unit']) :

                case"session":
                    $single = "session";
                    $plural = "session";

                    if($script['duration'] == 1){ echo $strings[$single];}else{echo $strings[$plural];};

                    break;

                case"s":
                    $single = "second";
                    $plural = "seconds";

                    if($script['duration'] == 1){ echo $strings[$single];}else{echo $strings[$plural];};
                    break;

                case"h":
                    $single = "hour";
                    $plural = "hours";

                    if($script['duration'] == 1){ echo $strings[$single];}else{echo $strings[$plural];};
                    break;

                case"d":
                    $single = "day";
                    $plural = "days";

                    if($script['duration'] == 1){ echo $strings[$single];}else{echo $strings[$plural];};
                    break;

                case"m":
                    $single = "month";
                    $plural = "months";

                    if($script['duration'] == 1){ echo $strings[$single];}else{echo $strings[$plural];};
                    break;
                case"y":
                    $single = "year";
                    $plural = "years";

                    if($script['duration'] == 1){ echo $strings[$single];}else{echo $strings[$plural];};
                    break;
                default:
                    break;

            endswitch;
            ?>

        </div>

        <?php echo $script['description'];?>
    </div>
</div>
<?php endif;?>
