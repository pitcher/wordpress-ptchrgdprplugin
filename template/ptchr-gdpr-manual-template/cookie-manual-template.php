<?php
/*
Getting started:

To start with a manual cookie appearance, copy the ptchr-gdpr-manual-template folder into the root of your active theme.
Make sure to copy all files in the folder, from there on you can customize the plugin.



Default Translations:
If you want to use the plugin defined translations you can use the $strings = PtchrGdpr::strings() function.

Necessary  items:

to save cookies:

   <button id="cookies-submit-preferences" data-saved="<?php echo $strings['saved'];?>"><?php echo $strings['save-preferences'];?></button>


to toggle the detail view:
    <button id="ptchr-gdpr-show-details" data-string="<?php echo $strings['show-details'];?>"><?php echo $strings['show-details']?></button>

The button above toggles the next div:
 <div class="ptchr-gdprcookie__details" id="ptchrgdprdetails">
*/


$strings = PtchrGdpr::strings();

?>
<div class="ptchr-gdprcookiecontainer ptchr-gdprcookiecontainer--manual">

    <div class="ptchr-gdprcookie__main">

        <div class="ptchr-gdprcookie__title">
            <h3><?php echo $strings['title'];?></h3>
        </div>

        <div class="ptchr-gdprcookie__content">
            <?php echo $strings['content-advanced'];?>
        </div>

        <div class="ptchr-gdprcookie__selectors">

            <div class="selectorrow">

                <input type="checkbox" id="ptchrgdprnoodzakelijk" name="noodzakelijk" checked disabled>
                <label for="ptchrgdprnoodzakelijk"> <?php echo $strings['necessary-title'];?></label>


            </div>

            <div class="selectorrow">

                <input type="checkbox" id="ptchrgdprvoorkeuren" name="voorkeuren">
                <label for="ptchrgdprvoorkeuren"><?php echo $strings['preferences-title'];?></label>


            </div>

            <div class="selectorrow">

                <input type="checkbox" id="ptchrgdprstatistieken" name="statistieken">
                <label for="ptchrgdprstatistieken"><?php echo $strings['statistics-title'];?></label>

            </div>

            <div class="selectorrow">

                <input type="checkbox" id="ptchrgdprmarketing" name="marketing">
                <label for="ptchrgdprmarketing"><?php echo $strings['marketing-title'];?></label>


            </div>

        </div>
    </div>
    <button id="ptchr-gdpr-show-details" data-string="<?php echo $strings['show-details'];?>"><?php echo $strings['show-details']?></button>
    <button id="cookies-submit-preferences" data-saved="<?php echo $strings['saved'];?>"><?php echo $strings['save-preferences'];?></button>

    <div class="ptchr-gdprcookie__details" id="ptchrgdprdetails">

        <?php require_once('cookiedetails.php')?>

    </div>
</div>

