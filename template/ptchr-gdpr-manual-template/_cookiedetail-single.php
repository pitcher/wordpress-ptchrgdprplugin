<div class="cookiedetail__single">

    <div class="cookiedetail__content">
        <?php echo $script['name'];?>
    </div>

    <div class="cookiedetail__content">
        <?php echo $script['duration'];?> <?php echo $script['duration_unit'];?>
    </div>

    <div class="cookiedetail__content">
        <?php
        // Output label translations
        switch ($script['script_type']) :
            case 0: echo $strings['necessary-title'];      break;
            case 1: echo $strings['preferences-title'];    break;
            case 2: echo $strings['statistics-title'];     break;
            case 3: echo $strings['marketing-title'];      break;
        endswitch;

        ?>
    </div>
    <div class="cookiedetail__content cookiedetail__content--large">
        <?php echo $script['description'];?>
    </div>
</div>
