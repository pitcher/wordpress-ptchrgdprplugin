<?php

$ptchrgdpr = new PtchrGdpr();
$ptchrgdpr->setScripts();
$scripts = $ptchrgdpr->getAllScripts();
$strings = $ptchrgdpr->strings();
?>

<div class="explanationrow">
    <div class="explanation"><?php echo $strings['necessary-explanation'];?></div>
    <div class="explanation"><?php echo $strings['preferences-explanation'];?></div>
    <div class="explanation"><?php echo $strings['statistics-explanation'];?></div>
    <div class="explanation"><?php echo $strings['marketing-explanation'];?></div>
</div>

<?php if(is_array($scripts)): ?>
<div class="cookiedetails__title"><h3><?php echo $strings['our-cookies'];?></h3></div>
<div class="cookiedetails">

    <div class="cookiedetail__title"><?php echo $strings['name'];?></div>
    <div class="cookiedetail__title"><?php echo $strings['duration'];?></div>
    <div class="cookiedetail__title"><?php echo $strings['type'];?></div>
    <div class="cookiedetail__title cookiedetail__title--large"><?php echo $strings['description'];?></div>


    <?php foreach ($scripts as $script ) : require('_cookiedetail-single.php'); endforeach; ?>


</div>
<?php endif; ?>
