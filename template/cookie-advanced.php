<?php if ( !defined('ABSPATH') ) {exit; };

$strings = PtchrGdpr::strings();

?>
<div class="ptchr-gdprcookiecontainer ptchr-gdprcookiecontainer--advanced animateinup">

    <div class="ptchr-gdprcookie__main">
        <div class="ptchr-gdprcookie__title">
            <h3><?php echo $strings['title']; ?></h3>
        </div>
        <div class="ptchr-gdprcookie__content">
            <?php echo $strings['content-advanced']; ?>
        </div>
        <div class="ptchr-gdprcookie__selectors">
            <div class="selectorrow">
                <label class="switch" for="ptchrgdprnoodzakelijk">
                    <input type="checkbox" id="ptchrgdprnoodzakelijk" name="noodzakelijk" checked disabled>
                    <span class="slider round"></span>
                </label>
                <span>  <?php echo $strings['necessary-title']; ?></span>

            </div>
            <div class="selectorrow">
                <label class="switch" for="ptchrgdprvoorkeuren">
                    <input type="checkbox" id="ptchrgdprvoorkeuren" name="voorkeuren">
                    <span class="slider round"></span>
                </label>
                <span>  <?php echo $strings['preferences-title']; ?></span>

            </div>
            <div class="selectorrow">
                <label class="switch" for="ptchrgdprstatistieken">
                    <input type="checkbox" id="ptchrgdprstatistieken" name="statistieken" data-toggle="toggle">
                    <span class="slider round"></span>
                </label>
                <span><?php echo $strings['statistics-title']; ?></span>

            </div>
            <div class="selectorrow">
                <label class="switch" for="ptchrgdprmarketing">
                    <input type="checkbox" id="ptchrgdprmarketing" name="marketing" data-toggle="toggle">
                    <span class="slider round"></span>
                </label>

                <span><?php echo $strings['marketing-title']; ?></span>
            </div>
            <div class="buttoncontainer">
                <button id="cookies-submit-agree" data-saved="<?php echo $strings['saved']; ?>"><?php echo $strings['accept-all']; ?></button>
                <button id="cookies-submit-preferences"
                        data-saved="<?php echo $strings['saved']; ?>"><?php echo $strings['save-preferences']; ?></button>
            </div>
        </div>
    </div>

    <button id="ptchr-gdpr-show-details"
            data-string='<?php echo $strings['show-details']; ?>'><?php echo $strings['show-details'] ?></button>
    <!--    <div class="toggleiconshowdetails"><img src="-->
    <?php //echo PTCHR_GDPR_PLUGIN_DIRECTORY ;?><!--template/assets/svg/chevron-arrow-down.svg"> </div>-->


    <div class="ptchr-gdprcookie__details" id="ptchrgdprdetails">

        <?php require_once('cookiedetails.php'); ?>

    </div>
</div>

