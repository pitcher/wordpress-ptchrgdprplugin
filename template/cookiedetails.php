<?php
if ( !defined('ABSPATH') ) {exit; };
$ptchrgdpr = new PtchrGdpr();

$ptchrgdpr->setScripts();
$scripts = $ptchrgdpr->getAllScripts();
$strings = $ptchrgdpr->strings();
?>

<div class="explanationrow">
    <ul>
    <li class="explanation"><?php echo $strings['necessary-explanation'];?></li>
    <li class="explanation"><?php echo $strings['preferences-explanation'];?></li>
    <li class="explanation"><?php echo $strings['statistics-explanation'];?></li>
    <li class="explanation"><?php echo $strings['marketing-explanation'];?></li>
    </ul>
</div>

<?php if(is_array($scripts)): ?>
<div class="cookiedetails__title"><h3><?php echo $strings['our-cookies'];?></h3></div>
<div class="cookiedetails">

    <div class="cookiedetail__title"><?php echo $strings['name'];?></div>
    <div class="cookiedetail__title"> <?php echo $strings['type'];?></div>

    <?php foreach ($scripts as $script ) : require('_cookiedetail-single.php'); endforeach; ?>


</div>
<?php endif; ?>
