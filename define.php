<?php if ( !defined('ABSPATH') ) {exit; };

define('PTCHR_GDPR_PLUGIN_DIRECTORY', plugin_dir_url(__FILE__));
define('PTCHR_GDPR_PLUGIN_PATH', plugin_dir_path(__FILE__));

if(defined('DISABLEALLOWLARAVEL')):
    add_filter( 'allowed_http_origins', 'add_allowed_origins' );
endif;



function add_allowed_origins( $origins ) {

    if( null == LARAVELBASE ):return $origins; endif;
    if(defined('HEADERALLOWALL')):
      $origins[] = "*";
    elseif(!isset($_SERVER['Access-Control-Allow-Origin']) || $_SERVER['HTTP_ORIGIN'] !== ''):

        $origins[] = LARAVELBASE;

    endif;

    return $origins;
}
