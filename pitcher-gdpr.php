<?php
/**
 * Plugin Name:     GDPR/AVG By Ptchr
 * Plugin URI:      ptchr.nl
 * Description:     Plaats scripts, beacons en tracking pixels, stel in welke elementen je optin/optout wil hebben.
 * Author:          Ptchr
 * Author URI:      ptchr.nl
 * Text Domain:     pitcher-gdpr
 * Domain Path:     /languages
 * Version:         1.0.2 
 *
 * @package         Pitcher_Gdpr
 */


// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
};


class PtchrGdpr
{
    private $sortedscripts = array();
    private $definedcookiedescriptions = array();
    private $presettags = array();


    function __construct()
    {

        require_once('define.php');
        require_once('lib/functions.php');

        require_once('lib/loadacfplugin.php');
        require_once('lib/customposttypescript.php');
        require_once('lib/createoptionspage.php');
        require_once('lib/templateselector.php');
        require_once('lib/presetcookiedefinitions.php');

        add_action('init', array($this, 'init'), 5);
        add_action('wp_enqueue_scripts', 'templateSelector');
        add_action('save_post', array($this, 'resetTransients'), 5);
        add_action('wp_head', array($this, 'outputPageId'));
        add_action('wp_head', array($this, 'ptchr_ajaxurl'));


        load_theme_textdomain('ptchr-gdpr', plugin_dir_path(__FILE__) . '/languages');


    }


    static function strings()
    {

        $strings = array(

            'year' => __('year', 'ptchr-gdpr'),
            'years' => __('years', 'ptchr-gdpr'),
            'month' => __('month', 'ptchr-gdpr'),
            'months' => __('months', 'ptchr-gdpr'),
            'day' => __('day', 'ptchr-gdpr'),
            'days' => __('days', 'ptchr-gdpr'),
            'hour' => __('hour', 'ptchr-gdpr'),
            'hours' => __('hours', 'ptchr-gdpr'),
            'second' => __('second', 'ptchr-gdpr'),
            'seconds' => __('seconds', 'ptchr-gdpr'),
            'session' => __('session', 'ptchr-gdpr'),

            'name' => __('Name', 'ptchr-gdpr'),
            'duration' => __('Retention period', 'ptchr-gdpr'),
            'type' => __('Type', 'ptchr-gdpr'),
            'description' => __('Description', 'ptchr-gdpr'),
            'our-cookies' => __('Our Cookies', 'ptchr-gdpr'),
            'saved' => __('Saving...', 'ptchr-gdpr'),

            'necessary-title' => __('Necessary', 'ptchr-gdpr'),
            'preferences-title' => __('Preferences', 'ptchr-gdpr'),
            'statistics-title' => __('Statistics', 'ptchr-gdpr'),
            'marketing-title' => __('Marketing', 'ptchr-gdpr'),

            'title' => get_field('cookiehandler_title', 'options') ?: __('This site uses cookies', 'ptchr-gdpr'),
            'content-advanced' => get_field('cookiehandler_message', 'options') ?: __('Welcome! This website uses cookies to guarantee a working website, to give us insights in our visitors and to help us improve the website. By accepting and saving your preferences you agree to our privacystatement. ', 'ptchr-gdpr'),
            'content-basic' => get_field('cookiehandler_message', 'options') ?: __('By accepting and saving your preferences you agree to our <a href="/privacy" target="_blank">privacystatement</a>.', 'ptchr-gdpr'),

            'save-preferences' => get_field('cookiehandler_save-preferences', 'options') ?: __('Accept selection', 'ptchr-gdpr'),
            'show-details' => get_field('cookiehandler_show-details', 'options') ?: __('Show details', 'ptchr-gdpr'),
            'accept-all' => __('Accept all', 'ptchr-gdpr'),
            'agree' => get_field('cookiehandler_agree', 'options') ?: __('I consent', 'ptchr-gdpr'),
            'disagree' => get_field('cookiehandler_disagree', 'options') ?: __('I do not consent', 'ptchr-gdpr'),

            'necessary-explanation' => get_field('cookiehandler_necessary-explanation', 'options') ?: __('Necessary cookies and scripts are necessary to guarantee a functional website. We never collect or process user data with these cookies.', 'ptchr-gdpr'),
            'preferences-explanation' => get_field('cookiehandler_preferences-explanation', 'options') ?: __('Preferential Cookies help us save your preferences like language, location or specific settings.', 'ptchr-gdpr'),
            'statistics-explanation' => get_field('cookiehandler_statistics-explanation', 'options') ?: __('Statistical Cookies help us analyze the pages that are visited the most, or the least. This information is anonymized before it is processed.', 'ptchr-gdpr'),
            'marketing-explanation' => get_field('cookiehandler_marketing-explanation', 'options') ?: __('Marketing Cookies are used to show you embeds from other sites like Youtube, Facebook, Twitter, These cookies can alse be used to show you personalised advertisements.', 'ptchr-gdpr'),

            'doubleclick' => __('Doubleclick is a service with which advertisers see how often and where ads are shown and seen. Cookies placed by Doubleclick are thus used primarely for marketing.
                Advertisers can use doubleclick to show ads to the visitor. The cookieduration is set to 1 year, Doubleclick does not collect personal information.

                Google owns Doubleclick, <a href="https://policies.google.com/privacy" target="_blank">for more information about how Google uses Doubleclick please see Google\'s privacy Policy here. </a>', 'ptchr-gdpr'),
            'facebookpixel' => __('A Facebook Pixel is a tiny bit of code that allows us to monitor the activity of our visitors on this website. With this code we can quantize page views, clicks and other activities on the website. Facebook Pixel is also used to monitor the effectivity of our advertisements, to specify target demographics and to adjust our campaigns to target demographics.

                Facebook (can) use multiple different tracking cookies:

                - datr : ( 2 years )
                - c_user ( depending on your settings in Facebook, one month or until session ends )
                - fr ( 3 months )
                - _fbp ( 3 months )

                The Facebook Pixel collects HTTP headers, Pixel ID, Facebook Cookie in case you are logged in in Facebook, button activities and field names in forms.
                <a href="https://developers.facebook.com/docs/facebook-pixel" target="_blank" rel="noopener">For more information Visit the Facebook Pixel detail page here.</a>
                <a href="https://www.facebook.com/about/privacy/update" target="_blank" rel="noopener">View Facebook\'s Privacy Policy here.</a>
                ', 'ptchr-gdpr'),
            'googleanalytics' => __('Google Analytics is a cookie used for analytical or statistical purposes with which we can see visitor flows and pageviews that our visitors make.

                We use Google Analytics to gain insights in visitor flows, traffic flows and pageviews on our website. Google never processes your full IP-address and maintains your anonimity as much as possible. For example, we cant see what a individual visits on our website. Google can be asked to give third parties access to this data if required by law, or when third parties are allowed to process this information.

                <a href="https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage" target="_blank">For more information about how Google Analytics uses cookies click here.</a>. </br>
                <a href="https://adssettings.google.com/authenticated" target="_blank">To see what settings Google uses in regard to (personalised) advertisements, click here.</a>

                Cookies:
                _ga : expires after 2 years
                _gat: expires after 2 minutes
                _gid: expires after two days
                ', 'ptchr-gdpr'),
            'ptchrgdprplugin' => __('Cookiepreferences

                Your cookie preferences are saved in the following cookies and are neccesary to help us see what you allow in terms of cookies and scripts. </br>
                Cookie-preferences-submitted & Cookie-settings', 'ptchr-gdpr'),
            'sharethis' => __('Cookie: Sharethis


                Cookienaam: __Sharethis_cookie_test__ & __stid
                Cookie duratie : Sessie & 1j
                beschrijving:
                Sharethis is een bedrijf dat zich bezig houdt met data analyse, ze verwerken informatie met de sharing tools die zij aanbieden. Met de sharing tools kun je makkelijk pagina\'s en links delen op social media.


                Sharethis is een bedrijf dat zich bezig houdt met data analyse, ze verwerken informatie met de sharing tools die zij aanbieden. Met de sharing tools kun je makkelijk pagina\'s en links delen op social media.
                <br/>Cookies: __Sharethis_cookie_test__ (sessie) & __stid ( 1 jaar )
                ', 'ptchr-gdpr'),


        );


        return $strings;

    }


    function outputPageId()
    {
        if (!is_admin()):
            ?>
<script type="text/javascript">
	var ptchrgdprpostid = '<?php global $post; if (isset($post->ID)) : echo $post->ID; else: echo ' - 1 ';endif;?>';

</script>
<?php
        endif;

    }


    function ptchr_ajaxurl()
    {

        echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
    }

    public function basePackage()
    {

        $this->setScripts();

        ob_start(); ?>
<script>
	var ptchrgdprscripts = < ? php echo json_encode($this - > getGdprPackage()) ? > ;

</script>
<script src="<?php echo plugin_dir_url(__FILE__) ?>lib/scripts.js"></script>
<script src="<?php echo plugin_dir_url(__FILE__) ?>lib/vendor/cookie.js"></script>
<script src="<?php echo plugin_dir_url(__FILE__) ?>lib/vendor/bootstrap-toggle.min.js"></script>
<link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__) ?>template/assets/style.css" />
<link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__) ?>template/assets/vendor/bootstrap-toggle.min.css" />
<script src="<?php echo plugin_dir_url(__FILE__) ?>template/js/rendercookiebasic.js"></script>
<?php

        return ob_get_clean();

    }

    public function init()
    {


        if (is_admin()):
            return false;
        endif;


        $this->setScripts();

        wp_enqueue_script('jquery', plugin_dir_url(__FILE__) . 'lib/vendor/jquery.min.js', '', '3.3.1');
        wp_enqueue_script('pitcher-gdpr-cookie-library', plugin_dir_url(__FILE__) . 'lib/vendor/cookie.js');
        wp_enqueue_script('pitcher-gdpr', plugin_dir_url(__FILE__) . 'lib/scripts.js');
        wp_enqueue_script('bootstrap-toggle', plugin_dir_url(__FILE__) . 'lib/vendor/bootstrap-toggle.min.js', '', '1', true);
        wp_enqueue_style('pitcher-gdpr', plugin_dir_url(__FILE__) . 'template/assets/style.css', array(), '');
        wp_enqueue_style('bootstrap-toggle', plugin_dir_url(__FILE__) . 'template/assets/vendor/bootstrap-toggle.min.css', array(), '');

        wp_localize_script('pitcher-gdpr', 'ptchrgdprscripts', $this->getGdprPackage());


        //todo-ptchr : add transients and test this
        //wp_localize_script( 'pitcher-gdpr', 'ptchrgdprscripts', get_transient('gdpr_ptchr_scripts'));


    }


    // IF SCRIPT IS ADDED, CHANGED OR REMOVED, RENEW TRANSIENT
    public function resetTransients($post_id)
    {
        if (get_post_type($post_id) == 'scripts') {
            delete_transient('gdpr_ptchr_scripts');
        };
        return;
    }

    public function setScripts()
    {
        $strings = $this->strings();
        //todo-ptchr : add transients and test this
        //if (false === get_transient('gdpr_ptchr_scripts')) :


        $scriptargs = array(
            'posts_per_page' => -1,
            'post_type' => 'scripts',
            'post_status' => 'publish'
        );

        foreach (get_posts($scriptargs) as $key => $script):

            if (get_field('active', $script->ID)):

                $description = get_field('description', $script->ID);


                // IF User specified predefined text ( standard cookie description  SCD,)
                // for description then load that text into the cookie instead.

                if (isset ($strings[get_field('voorgefinieerde_beschrijvingen', $script->ID)])) :

                    $description = nl2br(__($strings[get_field('voorgefinieerde_beschrijvingen', $script->ID)], 'ptchr-gdpr'));

                endif;


                $s = array(
                    'name' => get_field('scriptname', $script->ID),
                    'visible' => get_field('visible', $script->ID),
                    'position' => get_field('position', $script->ID),
                    'script_type' => get_field('script_type', $script->ID),
                    'duration' => get_field('duration', $script->ID),
                    'duration_unit' => get_field('duration_unit', $script->ID),
                    'location_on_site' => get_field('location_on_site', $script->ID),
                    'description' => $description,
                    'code' => get_field('code', $script->ID),
                );
                array_push($this->sortedscripts, $s);
            endif;

        endforeach;


        //todo-ptchr : add transients and test this
        //set_transient('gdpr_ptchr_scripts', $this->sortedscripts, 12 * HOUR_IN_SECONDS);
        //endif;

    }

    private function getPredifinedDescription($scd)
    {


    }

    public function getGdprPackage()
    {
        global $post;
        $find = array('http://', 'https://', 'www');
        $baseurl = str_replace($find, '', get_home_url());
        //$subdomains = get_field('subdomains','options');

        return array(
            'scripts' => $this->getAllScripts(),
            'cookiehash' => get_field('cookiemelding_hash', 'options'),
            'active' => get_field('cookiehandler_active', 'options'),
            'baseurl' => $baseurl,
            'ajaxurl' => admin_url('admin-ajax.php', 'relative'),

        );
    }


    public function getAllScripts()
    {

        return $this->sortedscripts;

    }


//    function getCookiedetails() {
//        ob_start();
//        require_once(PTCHR_GDPR_PLUGIN_DIRECTORY.'templates/cookiedetails.php');
//        return ob_get_clean();
//    };
//add_shortcode('ptchrgdprcookies', 'getCookiedetails');


}

$ptchrGdpr = new PtchrGdpr();
