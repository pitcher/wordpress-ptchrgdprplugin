<?php

function ptchr_gdpr_transient_cleared()
{

    echo '<div class="error notice">';
        echo "<p>" . _e( 'GDPR Cache bijgewerkt.', 'ptchr-gdpr' ) . "</p>";
    echo "</div>";

}




function getCookiedetailsoverview() {
    ob_start();
    
    require(  PTCHR_GDPR_PLUGIN_PATH .'template/shortcodecookiedetails.php');

    $cookiehtml = ob_get_clean();
    return $cookiehtml;
};

//getCookiedetailsoverview();
add_shortcode('ptchrgdprcookies', 'getCookiedetailsoverview');
