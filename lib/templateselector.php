<?php

function templateSelector(){

    if (get_field('cookiehandler_active', 'options')
        && get_field('cookiehandler_type', 'options') == 0)
    {
        wp_enqueue_script( 'rendercookiebasic', plugin_dir_url( __DIR__ ) . 'template/js/rendercookiebasic.js', array(), '1.0.0', true );

        //
    }

    if (get_field('cookiehandler_active', 'options')
        && get_field('cookiehandler_type', 'options') == 1)
    {
        wp_enqueue_script( 'rendercookieadvanced', plugin_dir_url( __DIR__ ) . 'template/js/rendercookieadvanced.js', array(), '1.0.0', true );
        //( __DIR__ . '/../template/cookie-advanced.php');
    }

    if (get_field('cookiehandler_active', 'options')
        && get_field('cookiehandler_type', 'options') == 2)
    {
        wp_enqueue_script( 'rendercookiemanual', plugin_dir_url( __DIR__ ) . 'template/js/rendercookiemanual.js', array(), '1.0.0', true );
        //( __DIR__ . '/../template/cookie-advanced.php');
    }


}

add_action( 'wp_ajax_load_cookie_basic', 'load_cookie_basic' );
add_action( 'wp_ajax_nopriv_load_cookie_basic', 'load_cookie_basic' );
function load_cookie_basic() {

    require_once( __DIR__ . '/../template/cookie-basic.php');
    die();
}

add_action( 'wp_ajax_load_cookie_advanced', 'load_cookie_advanced' );
add_action( 'wp_ajax_nopriv_load_cookie_advanced', 'load_cookie_advanced' );
function load_cookie_advanced() {

    require_once( __DIR__ . '/../template/cookie-advanced.php');

    die();
}


add_action( 'wp_ajax_load_cookie_manual', 'load_cookie_manual' );
add_action( 'wp_ajax_nopriv_load_cookie_manual', 'load_cookie_manual' );

function load_cookie_manual() {

    if(file_exists(get_template_directory().'/ptchr-gdpr-manual-template/cookie-manual-template.php')){
        require_once( get_template_directory().'/ptchr-gdpr-manual-template/cookie-manual-template.php');

    };

    die();
}

