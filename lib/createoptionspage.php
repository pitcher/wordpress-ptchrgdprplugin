<?php
if( function_exists('acf_add_options_page') ) {

       $option_sub_page = acf_add_options_sub_page(
        array(
            'page_title'  => 'Script & Cookie options',
            'menu_title'  => 'Script & Cookie options',
            'menu_slug'   => 'script-options',
            'parent_slug'   => 'edit.php?post_type=scripts',
            'capability' => 'manage_options',
            'autoload'    => true,
            'redirect'  => false
        ));

}
