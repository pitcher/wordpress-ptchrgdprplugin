<?php

function register_script_post_type()
{
// Register Custom Post Type

    // IF MENU admin.php?page=pitcher-builder-theme-settings exists, then place scripts there
    // otherwise place them in the default position.
    $parentmenu = true;


    $labels = array(
        'name' => _x('Scripts, Beacons and Cookies', 'Post Type General Name', 'ptchr-gdpr'),
        'singular_name' => _x('Script', 'Post Type Singular Name', 'ptchr-gdpr'),
        'menu_name' => __('Scripts & Cookies', 'ptchr-gdpr'),
        'name_admin_bar' => __('Post Type', 'ptchr-gdpr'),
        'archives' => __('Item Archives', 'ptchr-gdpr'),
        'attributes' => __('Item Attributes', 'ptchr-gdpr'),
        'parent_item_colon' => __('Parent Item:', 'ptchr-gdpr'),
        'all_items' => __('All scripts and cookies', 'ptchr-gdpr'),
        'add_new_item' => __('Add New Item', 'ptchr-gdpr'),
        'add_new' => __('Add New', 'ptchr-gdpr'),
        'new_item' => __('New Item', 'ptchr-gdpr'),
        'edit_item' => __('Edit Item', 'ptchr-gdpr'),
        'update_item' => __('Update Item', 'ptchr-gdpr'),
        'view_item' => __('View Item', 'ptchr-gdpr'),
        'view_items' => __('View Items', 'ptchr-gdpr'),
        'search_items' => __('Search Item', 'ptchr-gdpr'),
        'not_found' => __('Not found', 'ptchr-gdpr'),
        'not_found_in_trash' => __('Not found in Trash', 'ptchr-gdpr'),
        'featured_image' => __('Featured Image', 'ptchr-gdpr'),
        'set_featured_image' => __('Set featured image', 'ptchr-gdpr'),
        'remove_featured_image' => __('Remove featured image', 'ptchr-gdpr'),
        'use_featured_image' => __('Use as featured image', 'ptchr-gdpr'),
        'insert_into_item' => __('Insert into item', 'ptchr-gdpr'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'ptchr-gdpr'),
        'items_list' => __('Items list', 'ptchr-gdpr'),
        'items_list_navigation' => __('Items list navigation', 'ptchr-gdpr'),
        'filter_items_list' => __('Filter items list', 'ptchr-gdpr'),
    );

    register_post_type('scripts', $args = array(
        'label' => __('Script', 'ptchr-gdpr'),
        'description' => __('Scripts, Beacons and Cookies', 'ptchr-gdpr'),
        'labels' => $labels,
        'supports' => array('title', 'revisions'),
        'hierarchical' => false,
        'public' => false,
        'show_ui' => true,
        'show_in_menu' => $parentmenu,
        'menu_position' => 99,
        'menu_icon'=>'dashicons-forms',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => false,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
        'show_in_rest' => false,
    ));
}


add_action('init', 'register_script_post_type', 5);
