// jQuery.noConflict();
cookiehash = ptchrgdprscripts['cookiehash'];
accepted = 'cookie-preferences-submitted';
accepted = accepted + cookiehash;
cookiesettings = 'cookie-settings';
cookiesettings = cookiesettings + cookiehash;

if (ptchrgdprscripts['baseurl'].charAt(0) != ".") {
    domain = "." + ptchrgdprscripts['baseurl'];
} else {
    domain = ptchrgdprscripts['baseurl'];
}

domain = window.location.hostname;

var parts = domain.split('.');
var tld = parts[parts.length - 1];
var domain = parts[parts.length - 2] + '.' + tld;

if (tld == 'uk' && parts[parts.length - 2 == "co"]) {
    //quick fix for possible .co.uk domains
    domain = parts[part.length - 3] + '.' + tld;
}


subdomains = ptchrgdprscripts['subdomains'];
expirycookie = 365; // in days

function isCookieOptIn(cookie) {


    if (typeof cookie != "undefined"
        && cookie['script_type'] !== undefined
        && parseInt(cookie['script_type']) === 0) {

        return false;

    }

    return true;

}

function isCookieOnThisPage(cookie) {
    if (typeof cookie == "undefined"
        || cookie['location_on_site'] == "undefined") {
        return false;
    }
    if (cookie['location_on_site'] === "") {
        return true;
    }

    if (cookie['location_on_site'].indexOf(parseInt(ptchrgdprpostid)) != -1) {
        //console.log('cookie had array of pages, this page was one of them.');
        //console.log(cookie['location_on_site']);
        //console.log(ptchrgdprpostid);
        return true;
    }

    return false;

}

function getUserPreferences() {

    console.log(Cookies.get(cookiesettings));
    if (typeof Cookies.get(cookiesettings) != "undefined") {

        usercookiepreferences = JSON.parse(Cookies.get(cookiesettings));
        console.log(usercookiepreferences);
        return "required";
    }
    return "required";
    console.log('none');
}

function hasUserAcceptedPreference(preference) {


    if (typeof Cookies.get(cookiesettings) == "undefined") {
        switch (preference) {
            case "required":
                return true;
                break;
            default:
                return false;
                break;
        }
    }

    ucp = JSON.parse(Cookies.get(cookiesettings));
    // 0 : Noodzakelijk
    // 1 : Voorkeuren
    // 2 : Statistieken
    // 3 : Marketing

    switch (preference) {
        case  "marketing":
            return ucp[3];
            break;

        case "statistics":
            return ucp[2];
            break;

        case "required":
        case "preferences":
            return true;
            break;

        default:
            return false;
    }

}

function renderCookieCode(cookie) {
    //console.log(cookie['position']);
    if (typeof cookie == "undefined"
        || cookie['position'] == "undefined") {
        return false;
    }

    code = cookie['code'];

    switch (cookie['position']) {
        case "header_1" :
            jQuery("head").append(code);
            break;

        case  "body_0" :
            jQuery("body").prepend(code);
            // code block
            break;

        case  "body_1" :
            jQuery("body").append(code);
            // code block
            break;

        case  "footer_1" :
            jQuery("footer").prepend(code);
            // code block
            break;
    }
}

function hasUserOptedIn(cookie) {

    usercookiepreferences = '';

    maindomaincookies = Cookies.get(cookiesettings);
    subdomaincookies = [];

    if (typeof Cookies.get(cookiesettings) != "undefined") {

        usercookiepreferences = JSON.parse(Cookies.get(cookiesettings));

    }

    if (typeof usercookiepreferences == "undefined"
        && typeof cookie['script_type'] != "undefined"
        && parseInt(cookie['script_type']) === 0) {
        return true;
    }

    if (typeof usercookiepreferences != "undefined" && typeof cookie['script_type'] != "undefined"
        && usercookiepreferences[parseInt(cookie['script_type'])]) {
        return true;
    }

}

window.addEventListener('DOMContentLoaded', function (event) {
    if (typeof ptchrgdprscripts['active'] != "undefined" && ptchrgdprscripts['active'] == "1") {
        if (typeof ptchrgdprscripts['scripts'] != "undefined" && ptchrgdprscripts['scripts'].length > 0) {

            ptchrgdprscripts['scripts'].forEach(function (script) {

                if (
                    (!isCookieOptIn(script) && isCookieOnThisPage(script)) ||
                    (hasUserOptedIn(script) && isCookieOnThisPage(script))

                ) {
                    renderCookieCode(script);
                }

            });
        }
    }
});

/*
 FROM HERE ON COOKIE MESSAGE LOGIC
 */


document.addEventListener("DOMContentLoaded", function (event) {
    if (typeof ptchrgdprscripts['active'] != "undefined" && ptchrgdprscripts['active'] == "1") {
        // jQuery('body').addClass('cookieopened');

        if (Cookies.get(accepted)) {
            // console.log('user has done something with the cookie message, it is now hidden');
            // helperusercookiepreferences = JSON.parse(Cookies.get(cookiesettings));
            //
            // var $cookiecontent = jQuery(".ptchr-gdprcookiecontainer").contents();
            // console.log($cookiecontent);
            //
            // if (helperusercookiepreferences[1]) {
            //     console.log(jQuery("#ptchrgdprvoorkeuren",$cookiecontent));
            //     jQuery("#ptchrgdprvoorkeuren",$cookiecontent).prop("checked", true);
            // }
            // if (helperusercookiepreferences[2]) {
            //     console.log('ptchrgdprstatistieken');
            //     jQuery("#ptchrgdprstatistieken",$cookiecontent).prop("checked", true);
            // }
            // if (helperusercookiepreferences[3]) {
            //     console.log('ptchrgdprmarketing');
            //     jQuery("#ptchrgdprmarketing",$cookiecontent).prop("checked", true);
            // }


        } else {
            //console.log('first visit');
            jQuery('body').addClass('cookieopened');
        }
        jQuery(document).on('click', '#ptchr-gdpr-show-details', function (e) {

            if (jQuery('.ptchr-gdprcookiecontainer').hasClass('details-opened')) {
                jQuery('.ptchr-gdprcookiecontainer').removeClass('details-opened');
                jQuery(this).html(jQuery("#ptchr-gdpr-show-details").data('string'));
            } else {
                jQuery(this).html('Verberg details');
                jQuery('.ptchr-gdprcookiecontainer').addClass('details-opened');

            }

        });
        jQuery(document).on('click', '.cookiedetail__single', function (e) {

            if (jQuery(this).hasClass('opened')) {
                jQuery(this).removeClass('opened');
            } else {
                jQuery(this).addClass('opened');
            }

        });


        jQuery(document).on('click', '.selectorrow', function (e) {

            if (jQuery(this).hasClass('opened')) {
                jQuery(this).removeClass('opened');
            } else {
                jQuery(this).addClass('opened');
            }

        });

        jQuery(document).on('click', '#cookies-submit-preferences', function (e) {
            /*

            ON click we check all the current values from the cookie message.

             */
            // 0 : Noodzakelijk
            // 1 : Voorkeuren
            // 2 : Statistieken
            // 3 : Marketing
            cookiesettingvalues = {
                0: true,
                1: jQuery("input[name*='voorkeuren']").is(':checked'),
                2: jQuery("input[name*='statistieken']").is(':checked'),
                3: jQuery("input[name*='marketing']").is(':checked')
            };

            Cookies.set(accepted, 'true', {domain: domain, expires: expirycookie});
            Cookies.set(cookiesettings, cookiesettingvalues, {domain: domain, expires: expirycookie});

            jQuery("#cookies-submit-preferences").html('' + jQuery("#cookies-submit-preferences").data('saved'));
            setTimeout(function () {
                jQuery('body').removeClass('cookieopened');
                location.reload();
            }, 1000);

        });


        //cookies-submit-all-cookies

        jQuery(document).on('click', '#cookies-submit-disagree', function (e) {


            /*

           ON click we disable all scripts

            */
            // 0 : Noodzakelijk
            // 1 : Voorkeuren
            // 2 : Statistieken
            // 3 : Marketing
            cookiesettingvalues = {
                0: true,
                1: false,
                2: false,
                3: false
            };

            Cookies.set(accepted, 'true', {domain: domain, expires: expirycookie});
            Cookies.set(cookiesettings, cookiesettingvalues, {domain: domain, expires: expirycookie});

            jQuery(this).html('' + jQuery(this).data('saved'));
            setTimeout(function () {
                jQuery('body').removeClass('cookieopened');
                location.reload();
            }, 1000);
        });


        jQuery(document).on('click', '#cookies-submit-agree', function (e) {


            /*
           ON click we set all values to true as the user accepts to all cookies and scripts
            */
            // 0 : Noodzakelijk
            // 1 : Voorkeuren
            // 2 : Statistieken
            // 3 : Marketing
            cookiesettingvalues = {
                0: true,
                1: true,
                2: true,
                3: true
            };

            Cookies.set(accepted, 'true', {domain: domain, expires: expirycookie});
            Cookies.set(cookiesettings, cookiesettingvalues, {domain: domain, expires: expirycookie});

            jQuery(this).html('' + jQuery(this).data('saved'));
            setTimeout(function () {
                jQuery('body').removeClass('cookieopened');
                location.reload();
            }, 1000);
        });

    }

    jQuery("a[href='#cookiewindow']").click(function () {

        helperusercookiepreferences = JSON.parse(Cookies.get(cookiesettings));

        var $cookiecontent = jQuery(".ptchr-gdprcookiecontainer").contents();


        if (helperusercookiepreferences[1]) {
            //console.log(jQuery("#ptchrgdprvoorkeuren"));
            jQuery("#ptchrgdprvoorkeuren").prop("checked", true);
        }
        if (helperusercookiepreferences[2]) {
            //console.log('ptchrgdprstatistieken');
            jQuery("#ptchrgdprstatistieken").prop("checked", true);
        }
        if (helperusercookiepreferences[3]) {
            //console.log('ptchrgdprmarketing');
            jQuery("#ptchrgdprmarketing").prop("checked", true);
        }

        jQuery('body').addClass('cookieopened');
    });

});

