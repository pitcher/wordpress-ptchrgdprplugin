<?php if ( !defined('ABSPATH') ) {exit; };

/*
We define the preset cookie definitions here so users can just select values from a dropdown instead of having to manually set everything.

googleanalytics : Google Analytics
googletagmanager : Google Tag Manager
googlemaps : Google Maps
youtube : Youtube
vimeo : Vimeo
doubleclick : Doubleclick
facebookpixel : Facebook Pixel
phpsessionid : PHP sessie ID
ptchrgdprplugin : Pitcher Gdpr Plugin
sharethis : ShareThis
 */

function predefinedCookieDescriptions($presettags, $cookiename){

    if( !isset($cookiename) OR !$cookiename): return false; endif;

    /*
    googletagmanager : Google Tag Manager
    googlemaps : Google Maps
    youtube : Youtube
    vimeo : Vimeo
    phpsessionid : PHP sessie ID
     */

    //$gafile = file_get_contents(PTCHR_GDPR_PLUGIN_PATH .'cookiepresets/en_googleanalytics.txt');
//    $predefinedCookieDescriptions = array(
//
//        'googleanalytics'   =>   __($gafile,'ptchr-gdpr'),
//        'doubleclick'       =>   __(file_get_contents(PTCHR_GDPR_PLUGIN_PATH .'cookiepresets/en_doubleclick.txt'),'ptchr-gdpr'),
//        'facebookpixel'     =>   __(file_get_contents(PTCHR_GDPR_PLUGIN_PATH .'cookiepresets/en_facebookpixel.txt'),'ptchr-gdpr'),
//        'ptchrgdprplugin'   =>   __(file_get_contents(PTCHR_GDPR_PLUGIN_PATH .'cookiepresets/en_googleanalytics.txt'),'ptchr-gdpr'),
//        'sharethis'         =>   __(file_get_contents(PTCHR_GDPR_PLUGIN_PATH .'cookiepresets/en_sharethis.txt'),'ptchr-gdpr'), //todo-ptchr : toevoegen fr en de sharethis
//        'phpsessionid'      =>   __('','ptchr-gdpr'),
//        'googletagmanager'  =>   __('','ptchr-gdpr'), //todo-ptchr : toevoegen google tag manager default text
//        'googlemaps'        =>   __('','ptchr-gdpr'), //todo-ptchr : toevoegen maps default text
//        'youtube'           =>   __('','ptchr-gdpr'), //todo-ptchr : toevoegen youtube
//        'vimeo'             =>   __('','ptchr-gdpr'), //todo-ptchr : toevoegen vimeo
//    );


    if(isset($presettags[$cookiename])){
        return $presettags[$cookiename];
    }
}



