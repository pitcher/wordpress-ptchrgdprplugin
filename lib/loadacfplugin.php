<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

/** Start: Detect ACF Pro plugin. Include if not present. */
if (!class_exists('acf')) { // if ACF Pro plugin does not currently exist
    /** Start: Customize ACF path */
    add_filter('acf/settings/path', 'ptchrgdpr_acf_settings_path');
    /** End: Customize ACF path */
    /** Start: Customize ACF dir */
    add_filter('acf/settings/dir', 'ptchrgdpr_acf_settings_dir');
    /** End: Customize ACF path */
    /** Start: Hide ACF field group menu item */
    //add_filter('acf/settings/show_admin', '__return_false');
    /** End: Hide ACF field group menu item */
    /** Start: Include ACF */
    include_once(plugin_dir_path(__DIR__) . 'acf/acf.php');

    // NO SAVE, ONLY LOAD.
    /** End: Include ACF */
    /** Start: Create JSON save point */
    //add_filter('acf/settings/save_json', 'ptchrgdpr_acf_json_save_point');


    /** End: Create JSON save point */
    /** Start: Create JSON load point */
    add_filter('acf/settings/load_json', 'ptchrgdpr_acf_json_load_point');
    /** End: Create JSON load point */
    /** Start: Stop ACF upgrade notifications */
    //add_filter('site_transient_update_plugins', 'ptchrgdpr_stop_acf_update_notifications', 11);

    /** End: Stop ACF upgrade notifications */
} else { // else ACF Pro plugin does exist
    /** Start: Create JSON load point */
    add_filter('acf/settings/load_json', 'ptchrgdpr_acf_json_load_point');
    /** End: Create JSON load point */
} // end-if ACF Pro plugin does not currently exist
/** End: Detect ACF Pro plugin. Include if not present. */

function ptchrgdpr_acf_settings_path($path)
{
    $path = plugin_dir_path( __DIR__ ) . 'acf/';
    return $path;
}



function ptchrgdpr_acf_settings_dir($path)
{
    $dir = plugin_dir_url( __DIR__ ) . 'acf/';
    return $dir;
}


/** Start: Function to create JSON load point */
function ptchrgdpr_acf_json_load_point($paths)
{

    $dir = plugin_dir_path( __DIR__ );
    //var_dump($dir);
    $paths[] = $dir. 'acf-json/';

    return $paths;
}
/** End: Function to create JSON load point */
function ptchrgdpr_acf_json_save_point($path)
{
    $path = plugin_dir_path( __DIR__ ) . 'acf-json/';
    return $path;
}
function ptchrgdpr_stop_acf_update_notifications($value)
{
    unset($value->response[plugin_dir_path( __DIR__ ) . 'acf/acf.php']);
    return $value;
}
