��    '      T  5   �      `  �  a  
   L     W  r   h  U  �  �   1
       �    �    	   �     	  	     �   $     �  	   �  �   �     d     p  `   |     �  	   �     �  �     
   �     �     �  �   �     �     �     �     �     �     �     �     �     �     �     �  �  �  Q  �     �     �  n   �  �  j  �   �     �  
  �  g  �     G%     U%  	   h%  �   r%     e&     j&  y   w&     �&  
   �&  �   	'     �'  
   �'     �'  |   �'     0(  #   =(     a(  2  f(     �)     �)     �)     �)     �)     �)     �)     �)     �)     �)     �)            
              &      "                     	                                             '            %               #            $             !                           A Facebook Pixel is a tiny bit of code that allows us to monitor the activity of our visitors on this website. With this code we can quantize page views, clicks and other activities on the website. Facebook Pixel is also used to monitor the effectivity of our advertisements, to specify target demographics and to adjust our campaigns to target demographics.

Facebook (can) use multiple different tracking cookies:

- datr : ( 2 years )
- c_user ( depending on your settings in Facebook, one month or until session ends )
- fr ( 3 months )
- _fbp ( 3 months )

The Facebook Pixel collects HTTP headers, Pixel ID, Facebook Cookie in case you are logged in in Facebook, button activities and field names in forms.
<a href="https://developers.facebook.com/docs/facebook-pixel" target="_blank" rel="noopener">For more information Visit the Facebook Pixel detail page here.</a>
<a href="https://www.facebook.com/about/privacy/update" target="_blank" rel="noopener">View Facebook's Privacy Policy here.</a>
 Accept all Accept selection By accepting and saving your preferences you agree to our <a href="/privacy" target="_blank">privacystatement</a>. Cookie: Sharethis


Cookienaam: __Sharethis_cookie_test__ & __stid
Cookie duratie : Sessie & 1j
beschrijving: 
Sharethis is een bedrijf dat zich bezig houdt met data analyse, ze verwerken informatie met de sharing tools die zij aanbieden. Met de sharing tools kun je makkelijk pagina's en links delen op social media.


Sharethis is een bedrijf dat zich bezig houdt met data analyse, ze verwerken informatie met de sharing tools die zij aanbieden. Met de sharing tools kun je makkelijk pagina's en links delen op social media. 
<br/>Cookies: __Sharethis_cookie_test__ (sessie) & __stid ( 1 jaar )
 Cookiepreferences

Your cookie preferences are saved in the following cookies and are neccesary to help us see what you allow in terms of cookies and scripts. </br>
Cookie-preferences-submitted & Cookie-settings Description Doubleclick is a service with which advertisers see how often and where ads are shown and seen. Cookies placed by Doubleclick are thus used primarely for marketing.
Advertisers can use doubleclick to show ads to the visitor. The cookieduration is set to 1 year, Doubleclick does not collect personal information.

Google owns Doubleclick, <a href="https://policies.google.com/privacy" target="_blank">for more information about how Google uses Doubleclick please see Google's privacy Policy here. </a> Google Analytics is a cookie used for analytical or statistical purposes with which we can see visitor flows and pageviews that our visitors make.

We use Google Analytics to gain insights in visitor flows, traffic flows and pageviews on our website. Google never processes your full IP-address and maintains your anonimity as much as possible. For example, we cant see what a individual visits on our website. Google can be asked to give third parties access to this data if required by law, or when third parties are allowed to process this information.

<a href="https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage" target="_blank">For more information about how Google Analytics uses cookies click here.</a>. </br>
<a href="https://adssettings.google.com/authenticated" target="_blank">To see what settings Google uses in regard to (personalised) advertisements, click here.</a>

Cookies:
_ga : expires after 2 years 
_gat: expires after 2 minutes
_gid: expires after two days
 I consent I do not consent Marketing Marketing Cookies are used to show you embeds from other sites like Youtube, Facebook, Twitter, These cookies can alse be used to show you personalised advertisements. Name Necessary Necessary cookies and scripts are necessary to guarantee a functional website. We never collect or process user data with these cookies. Our Cookies Preferences Preferential Cookies help us save your preferences like language, location or specific settings. Retention period Saving... Show details Statistical Cookies help us analyze the pages that are visited the most, or the least. This information is anonymized before it is processed. Statistics This site uses cookies Type Welcome! This website uses cookies to guarantee a working website, to give us insights in our visitors and to help us improve the website. By accepting and saving your preferences you agree to our privacystatement.  day days hour hours month months second seconds session year years Project-Id-Version: GDPR/AVG By Ptchr 0.9.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/pitcher-gdpr
POT-Creation-Date: 2019-04-10T13:30:25+00:00
PO-Revision-Date: 2019-04-10 15:34+0200
Last-Translator: 
Language-Team: 
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
X-Domain: ptchr-gdpr
Plural-Forms: nplurals=2; plural=(n != 1);
 Een Facebook Pixel is een klein stuk code dat ons in staat stelt om de bezoekersactiviteit in kaart te brengen op onze website. Met deze code kunnen we paginaweergaves,kliks en andere activiteiten op de website inzichtelijk maken. Ook wordt de Facebook Pixel gebruikt om de effectiviteit van onze advertenties in kaart te brengen, specifieke doelgroepen vast te stellen en om advertenties af te kunnen stellen op mogelijke doelgroepen.

Facebook hanteert drie verschillende tracking cookies, en kan deze gebruiken:

- datr :  ( 2 jaar )
- c_user ( afhankelijk van instellingen 1 maand of sessie )
- fr ( 3 maanden )
- _fbp ( 3 maanden )

De Facebook Pixel verzamelt http Headers, de Pixel Id en Facebook Cookie in het geval dat je ingelogd bent op Facebook, Knop-activiteiten, veldnamen in formulieren.
<a href="https://developers.facebook.com/docs/facebook-pixel" target="_blank" rel="noopener">Voor meer informatie bezoek de Facebook Pixel overzichtspagina hier.</a>
<a href="https://www.facebook.com/about/privacy/update" target="_blank" rel="noopener">Bekijk het privacybeleid van Facebook hier.</a>
 Alles accepteren Accepteer selectie Door op accepteren te klikken, ga je akkoord met onze <a href="/privacy" target="_blank">privacystatement</a>. Cookie: Sharethis

Cookienaam: __Sharethis_cookie_test__ & __stid
Cookie duratie : Sessie & 1j
beschrijving: 
 Sharethis is een bedrijf dat zich bezig houdt met data analyse, ze verwerken informatie met de sharing tools die zij aanbieden. Met de sharing tools kun je makkelijk pagina's en links delen op social media. 
<br/>Cookies: __Sharethis_cookie_test__ (sessie) & __stid ( 1 jaar )
 Cookievoorkeuren

De cookievoorkeuren worden opgeslagen in de volgende cookies en zijn noodzakelijk om je te assisteren in het maken van een privacybewustte keuze.</br>
Cookie-preferences-submitted & Cookie-settings Beschrijving Doubleclick is een dienst waarmee adverteerders kunnen inzien hoe vaak en waar advertenties getoond en gezien zijn. De cookies die hiervoor geplaatst worden vallen onder de categorie marketing. Google is eigenaar van Doubleclick, adverteerders kunnen Doubleclick gebruiken om advertenties te tonen.
De duratie van de cookie is 1 jaar. Doubleclick verzamelt hiermee geen persoonsgebonden gegevens.
Voor meer informatie raadpleeg het <a href="https://policies.google.com/privacy" target="_blank">privacybeleid van Google</a> Cookie: Google Analytics
Bewaartermijn: 2 jaar


Cookies:
_ga ( verloopt na 2 jaar ) 
_gat ( verloopt na 2 minuten )
_gid ( verloopt na 2 dagen )


Google Analytics is een analysecookie waarmee we bezoekersstromen en paginaweergaves in kaart brengen. </br>
<a href="https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage" target="_blank">Voor meer informatie klik hier</a>. </br>

Kijk hier voor meer informatie over Google Analytics en de gebruikte cookies: https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage

We gebruiken Google Analytics om inzicht te krijgen in o.a. bezoekersstromen, verkeersbronnen en paginaweergaves op onze website. Dit betekent dat Google optreedt als bewerker bij de verwerking van persoonsgegevens door deze website.

Google verwerkt nooit uw volledige IP-adres en waarborgt uw anonimiteit zoveel mogelijk. Wij zien bijvoorbeeld niet welke individuele bezoekers welke bezoeken aan onze website afleggen.

Google kan uw informatie slechts aan derden verschaffen indien het bedrijf hiertoe wettelijk wordt verplicht, of voor zover derden de informatie namens Google verwerken. Ga voor meer informatie hierover Google’s Privacy Policy.

Om te kijken welke instellingen Google hanteert omtrent (gepersonaliseerde) advertenties kunt u https://adssettings.google.com/authenticated raadplegen.
 Ik ga akkoord Ik ga niet akkoord Marketing Marketing cookies worden gebruikt om je ingesloten content van andere sites te tonen. Denk hierbij aan YouTube video's, Facebook share buttons, Twitter tweets. Deze cookies kunnen gebruikt worden om je gepersonaliseerde content aan te bieden. Naam Noodzakelijk Noodzakelijke cookies en scripts zijn nodig om een functionele site te garanderen. We verzamelen hiermee geen informatie. Onze Cookies Voorkeuren Voorkeurscookies helpen ons jouw voorkeuren op deze site op te slaan, denk hierbij aan talen, locaties of specifieke instellingen. Bewaartermijn Opslaan... Details tonen Statistische cookies helpen ons de bezoekersstromen in te zien, deze informatie wordt geanonimiseerd opgeslagen en verwerkt. Statistieken Deze site maakt gebruik van cookies Type Welkom! Deze website maakt gebruik van cookies om je een goed functionerende site aan te kunnen bieden, ook maken wij gebruik van cookies om inzichten te krijgen in onze bezoekers en hiermee verbeteren we onze website. Met het accepteren en opslaan van je voorkeuren ga je akkoord met de privacystatement.  dag dagen uur uur maand maanden seconde seconden sessie jaar jaren 