# Copyright (C) 2019 Ptchr
# This file is distributed under the same license as the GDPR/AVG By Ptchr plugin.
msgid ""
msgstr ""
"Project-Id-Version: GDPR/AVG By Ptchr 0.9.33\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/pitcher-gdpr\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2019-04-10T13:30:25+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.1.0\n"
"X-Domain: ptchr-gdpr\n"

#. Plugin Name of the plugin
msgid "GDPR/AVG By Ptchr"
msgstr ""

#. Plugin URI of the plugin
#. Author URI of the plugin
msgid "ptchr.nl"
msgstr ""

#. Description of the plugin
msgid "Plaats scripts, beacons en tracking pixels, stel in welke elementen je optin/optout wil hebben."
msgstr ""

#. Author of the plugin
msgid "Ptchr"
msgstr ""

#: lib/customposttypescript.php:13
msgctxt "Post Type General Name"
msgid "Scripts, Beacons and Cookies"
msgstr ""

#: lib/customposttypescript.php:14
msgctxt "Post Type Singular Name"
msgid "Script"
msgstr ""

#: lib/customposttypescript.php:15
msgid "Scripts & Cookies"
msgstr ""

#: lib/customposttypescript.php:16
msgid "Post Type"
msgstr ""

#: lib/customposttypescript.php:17
msgid "Item Archives"
msgstr ""

#: lib/customposttypescript.php:18
msgid "Item Attributes"
msgstr ""

#: lib/customposttypescript.php:19
msgid "Parent Item:"
msgstr ""

#: lib/customposttypescript.php:20
msgid "All scripts and cookies"
msgstr ""

#: lib/customposttypescript.php:21
msgid "Add New Item"
msgstr ""

#: lib/customposttypescript.php:22
msgid "Add New"
msgstr ""

#: lib/customposttypescript.php:23
msgid "New Item"
msgstr ""

#: lib/customposttypescript.php:24
msgid "Edit Item"
msgstr ""

#: lib/customposttypescript.php:25
msgid "Update Item"
msgstr ""

#: lib/customposttypescript.php:26
msgid "View Item"
msgstr ""

#: lib/customposttypescript.php:27
msgid "View Items"
msgstr ""

#: lib/customposttypescript.php:28
msgid "Search Item"
msgstr ""

#: lib/customposttypescript.php:29
msgid "Not found"
msgstr ""

#: lib/customposttypescript.php:30
msgid "Not found in Trash"
msgstr ""

#: lib/customposttypescript.php:31
msgid "Featured Image"
msgstr ""

#: lib/customposttypescript.php:32
msgid "Set featured image"
msgstr ""

#: lib/customposttypescript.php:33
msgid "Remove featured image"
msgstr ""

#: lib/customposttypescript.php:34
msgid "Use as featured image"
msgstr ""

#: lib/customposttypescript.php:35
msgid "Insert into item"
msgstr ""

#: lib/customposttypescript.php:36
msgid "Uploaded to this item"
msgstr ""

#: lib/customposttypescript.php:37
msgid "Items list"
msgstr ""

#: lib/customposttypescript.php:38
msgid "Items list navigation"
msgstr ""

#: lib/customposttypescript.php:39
msgid "Filter items list"
msgstr ""

#: lib/customposttypescript.php:43
msgid "Script"
msgstr ""

#: lib/customposttypescript.php:44
msgid "Scripts, Beacons and Cookies"
msgstr ""

#: lib/functions.php:7
msgid "GDPR Cache bijgewerkt."
msgstr ""

#: pitcher-gdpr.php:58
msgid "year"
msgstr ""

#: pitcher-gdpr.php:59
msgid "years"
msgstr ""

#: pitcher-gdpr.php:60
msgid "month"
msgstr ""

#: pitcher-gdpr.php:61
msgid "months"
msgstr ""

#: pitcher-gdpr.php:62
msgid "day"
msgstr ""

#: pitcher-gdpr.php:63
msgid "days"
msgstr ""

#: pitcher-gdpr.php:64
msgid "hour"
msgstr ""

#: pitcher-gdpr.php:65
msgid "hours"
msgstr ""

#: pitcher-gdpr.php:66
msgid "second"
msgstr ""

#: pitcher-gdpr.php:67
msgid "seconds"
msgstr ""

#: pitcher-gdpr.php:68
msgid "session"
msgstr ""

#: pitcher-gdpr.php:70
msgid "Name"
msgstr ""

#: pitcher-gdpr.php:71
msgid "Retention period"
msgstr ""

#: pitcher-gdpr.php:72
msgid "Type"
msgstr ""

#: pitcher-gdpr.php:73
msgid "Description"
msgstr ""

#: pitcher-gdpr.php:74
msgid "Our Cookies"
msgstr ""

#: pitcher-gdpr.php:75
msgid "Saving..."
msgstr ""

#: pitcher-gdpr.php:77
msgid "Necessary"
msgstr ""

#: pitcher-gdpr.php:78
msgid "Preferences"
msgstr ""

#: pitcher-gdpr.php:79
msgid "Statistics"
msgstr ""

#: pitcher-gdpr.php:80
msgid "Marketing"
msgstr ""

#: pitcher-gdpr.php:82
msgid "This site uses cookies"
msgstr ""

#: pitcher-gdpr.php:83
msgid "Welcome! This website uses cookies to guarantee a working website, to give us insights in our visitors and to help us improve the website. By accepting and saving your preferences you agree to our privacystatement. "
msgstr ""

#: pitcher-gdpr.php:84
msgid "By accepting and saving your preferences you agree to our <a href=\"/privacy\" target=\"_blank\">privacystatement</a>."
msgstr ""

#: pitcher-gdpr.php:86
msgid "Accept selection"
msgstr ""

#: pitcher-gdpr.php:87
msgid "Show details"
msgstr ""

#: pitcher-gdpr.php:88
msgid "Accept all"
msgstr ""

#: pitcher-gdpr.php:89
msgid "I consent"
msgstr ""

#: pitcher-gdpr.php:90
msgid "I do not consent"
msgstr ""

#: pitcher-gdpr.php:92
msgid "Necessary cookies and scripts are necessary to guarantee a functional website. We never collect or process user data with these cookies."
msgstr ""

#: pitcher-gdpr.php:93
msgid "Preferential Cookies help us save your preferences like language, location or specific settings."
msgstr ""

#: pitcher-gdpr.php:94
msgid "Statistical Cookies help us analyze the pages that are visited the most, or the least. This information is anonymized before it is processed."
msgstr ""

#: pitcher-gdpr.php:95
msgid "Marketing Cookies are used to show you embeds from other sites like Youtube, Facebook, Twitter, These cookies can alse be used to show you personalised advertisements."
msgstr ""

#: pitcher-gdpr.php:97
msgid ""
"Doubleclick is a service with which advertisers see how often and where ads are shown and seen. Cookies placed by Doubleclick are thus used primarely for marketing.\n"
"Advertisers can use doubleclick to show ads to the visitor. The cookieduration is set to 1 year, Doubleclick does not collect personal information.\n"
"\n"
"Google owns Doubleclick, <a href=\"https://policies.google.com/privacy\" target=\"_blank\">for more information about how Google uses Doubleclick please see Google's privacy Policy here. </a>"
msgstr ""

#: pitcher-gdpr.php:101
msgid ""
"A Facebook Pixel is a tiny bit of code that allows us to monitor the activity of our visitors on this website. With this code we can quantize page views, clicks and other activities on the website. Facebook Pixel is also used to monitor the effectivity of our advertisements, to specify target demographics and to adjust our campaigns to target demographics.\n"
"\n"
"Facebook (can) use multiple different tracking cookies:\n"
"\n"
"- datr : ( 2 years )\n"
"- c_user ( depending on your settings in Facebook, one month or until session ends )\n"
"- fr ( 3 months )\n"
"- _fbp ( 3 months )\n"
"\n"
"The Facebook Pixel collects HTTP headers, Pixel ID, Facebook Cookie in case you are logged in in Facebook, button activities and field names in forms.\n"
"<a href=\"https://developers.facebook.com/docs/facebook-pixel\" target=\"_blank\" rel=\"noopener\">For more information Visit the Facebook Pixel detail page here.</a>\n"
"<a href=\"https://www.facebook.com/about/privacy/update\" target=\"_blank\" rel=\"noopener\">View Facebook's Privacy Policy here.</a>\n"
""
msgstr ""

#: pitcher-gdpr.php:114
msgid ""
"Google Analytics is a cookie used for analytical or statistical purposes with which we can see visitor flows and pageviews that our visitors make.\n"
"\n"
"We use Google Analytics to gain insights in visitor flows, traffic flows and pageviews on our website. Google never processes your full IP-address and maintains your anonimity as much as possible. For example, we cant see what a individual visits on our website. Google can be asked to give third parties access to this data if required by law, or when third parties are allowed to process this information.\n"
"\n"
"<a href=\"https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage\" target=\"_blank\">For more information about how Google Analytics uses cookies click here.</a>. </br>\n"
"<a href=\"https://adssettings.google.com/authenticated\" target=\"_blank\">To see what settings Google uses in regard to (personalised) advertisements, click here.</a>\n"
"\n"
"Cookies:\n"
"_ga : expires after 2 years \n"
"_gat: expires after 2 minutes\n"
"_gid: expires after two days\n"
""
msgstr ""

#: pitcher-gdpr.php:126
msgid ""
"Cookiepreferences\n"
"\n"
"Your cookie preferences are saved in the following cookies and are neccesary to help us see what you allow in terms of cookies and scripts. </br>\n"
"Cookie-preferences-submitted & Cookie-settings"
msgstr ""

#: pitcher-gdpr.php:130
msgid ""
"Cookie: Sharethis\n"
"\n"
"\n"
"Cookienaam: __Sharethis_cookie_test__ & __stid\n"
"Cookie duratie : Sessie & 1j\n"
"beschrijving: \n"
"Sharethis is een bedrijf dat zich bezig houdt met data analyse, ze verwerken informatie met de sharing tools die zij aanbieden. Met de sharing tools kun je makkelijk pagina's en links delen op social media.\n"
"\n"
"\n"
"Sharethis is een bedrijf dat zich bezig houdt met data analyse, ze verwerken informatie met de sharing tools die zij aanbieden. Met de sharing tools kun je makkelijk pagina's en links delen op social media. \n"
"<br/>Cookies: __Sharethis_cookie_test__ (sessie) & __stid ( 1 jaar )\n"
""
msgstr ""
